import sys
import math

def populate_dataset(name_of_file) :
    data_list = [] 
    with open(name_of_file, 'r') as file :
        line = file.readline();
        eof = False
        while not eof :
            line = file.readline().split(',')
            if line == ['']:
                eof = True
            else :
                data_list.append((float(line[0]), line[1].strip('"')))
    return data_list

def median_evaluation(dataset) :
    length = len(dataset)
    if length % 2 != 0 :
        median = (dataset[int(length / 2)][0], int(length / 2))
    else :
        median = ((dataset[int((length / 2) - 1)][0] + dataset[int(length / 2)][0])/2, int((length / 2)-1), int(length / 2))
    print(str(median))
    return median

def interquartile_division(dataset) :
    median = median_evaluation(dataset)
    if len(median) == 2 :
        q1 = median_evaluation(dataset[0 : median[1]])[0]
        q3 = median_evaluation(dataset[median[1] + 1 : ])[0]
    else :
        q1 = median_evaluation(dataset[0 : median[2]])[0]
        q3 = median_evaluation(dataset[median[2] : ])[0]
    print(str(q1)+ ' '+str(q3)+' '+str(q3-q1))
    return q1, q3, q3 - q1

def anomaly_count(dataset) :
    tp, fp, tn, fn = 0, 0, 0, 0
    q1, q3, iqr = interquartile_division(dataset)
    for data in dataset :
        if data[0] < q1 - 1.5 * iqr  or data[0] > q3 + 1.5 * iqr :
            if 'yes' in data[1] :
                tp = tp + 1   
            else :
                fp = fp + 1   
        else :
            if 'no' in data[1] :
                tn = tn + 1 
            else :
                fn = fn + 1
    return tp, fp, tn, fn

def print_anomalies(tp, fp, tn, fn) :
    print('\n')
    print('> tp: ' + str(tp))
    print('> fp: ' + str(fp))
    print('> tn: ' + str(tn))
    print('> fn: ' + str(fn))
    
def evaluate_metrics(dataset) :
    tp, fp, tn, fn = anomaly_count(dataset)
    print_anomalies(tp, fp, tn, fn)
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    accuracy = (tp + tn) / (tp + fp + tn + fn)
    f1 = (2 * precision * recall) / (precision + recall)
    auc_roc = (tp / (2 * (tp + fn))) + (tn / (2 * (tn + fp)))
    mcc = ((tp * tn) - (fp * fn)) / (math.sqrt((tp + fp)*(tp + fn)*(tn + fp)*(tn + fn)))
    return (tp, fp, tn, fn, precision, recall, accuracy, f1, auc_roc, mcc)

def print_metrics(metrics) :
    print('\n')
    print('> precision: ' + str(metrics[4]))
    print('> recall: ' + str(metrics[5]))
    print('> accuracy: ' + str(metrics[6]))
    print('> f1: ' + str(metrics[7]))
    print('> auc_roc: ' + str(metrics[8]))
    print('> mcc: ' + str(metrics[9]))
    print('\n')
    
def main() :
    dataset = populate_dataset(sys.argv[1])
    dataset.sort()
    metrics = evaluate_metrics(dataset)
    print_metrics(metrics)

if __name__ == '__main__' :
    main()
