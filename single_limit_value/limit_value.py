import sys
import math
import threading

def populate_dataset(name_of_file) :
    list_of_data = [] 
    with open(name_of_file, 'r') as file :
        line = file.readline();
        eof = False
        while not eof :
            line = file.readline().split(',')
            if line == ['']:
                eof = True
            else :
                list_of_data.append([float(line[0]), line[1].strip('"')])
    return list_of_data

def print_dataset_info(dataset_length, min_score, max_score) :
    print('---> dataset popolato: ' + str(dataset_length) + ' elementi')
    print('---> score minimo: ' + str(min_score))
    print('---> score massimo: ' + str(max_score))

def higher_anomaly_computation(list_of_data, limit_value) :
    tp, fp, tn, fn = 0, 0, 0, 0
    for x in list_of_data :
        if(x[0] > limit_value and x[1] == 'yes' ) :
            tp = tp + 1
        elif(x[0] > limit_value and x[1] == 'no' ) :
            fp = fp + 1
        elif(x[0] <= limit_value and x[1] == 'no' ) :
            tn = tn + 1
        elif(x[0] <= limit_value and x[1] == 'yes' ) :
            fn = fn +1
    return tp, fp, tn, fn

def lower_anomaly_computation(list_of_data, limit_value) :
    tp, fp, tn, fn = 0, 0, 0, 0
    for x in list_of_data :
        if(x[0] < limit_value and x[1] == 'yes' ) :
            tp = tp + 1
        elif(x[0] < limit_value and x[1] == 'no' ) :
            fp = fp + 1
        elif(x[0] >= limit_value and x[1] == 'no' ) :
            tn = tn + 1
        elif(x[0] >= limit_value and x[1] == 'yes' ) :
            fn = fn +1
    return tp, fp, tn, fn

def statistics_computation(list_of_data, limit_value, mode) :
    if mode == 'high' :
        return higher_anomaly_computation(list_of_data, limit_value)
    elif mode == 'low' :
        return lower_anomaly_computation(list_of_data, limit_value)

def metrics_calculus(tp, fp, tn, fn, limit_value) :
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    accuracy = (tp + tn) / (tp + fp + tn + fn)
    f1 = (2 * precision * recall) / (precision + recall)
    f2 = (5 * precision * recall) / (4 * precision + recall)
    auc_roc = (tp / (2 * (tp + fn))) + (tn / (2 * (tn + fp)))
    mcc = ((tp * tn) - (fp * fn)) / (math.sqrt((tp + fp)*(tp + fn)*(tn + fp)*(tn + fn)))
    #       0    1   2   3         4          5       6        7
    return [mcc, f2, f1, accuracy, precision, recall, auc_roc, limit_value, tp, fp, tn, fn]

def metrics_evaluation(list_of_data, min_score, max_limit_value, increment, mode) :
    list_of_metrics = []
    limit_value = min_score
    while limit_value <= max_limit_value :
        tp, fp, tn, fn = statistics_computation(list_of_data, limit_value, mode)
        if tp * (tn + fp) * (tn + fn) != 0 :
            list_of_metrics.append(metrics_calculus(tp, fp, tn, fn, limit_value))
        else :
            print('-------> zero division avoided, limit = ' + str(limit_value))
        limit_value = limit_value + increment
    return list_of_metrics
    
def print_metrics(best_result) :
    print('---> tp: ' + str(best_result[8]))
    print('---> fp: ' + str(best_result[9]))
    print('---> tn: ' + str(best_result[10]))
    print('---> fn: ' + str(best_result[11]))
    print('\n')
    print('---> precision: ' + str(best_result[4]))
    print('---> recall: ' + str(best_result[5]))
    print('---> accuracy: ' + str(best_result[3]))
    print('---> f1: ' + str(best_result[2]))
    print('---> f2: ' + str(best_result[1]))
    print('---> mcc: ' + str(best_result[0]))
    print('---> auc_roc: ' + str(best_result[6]))
    print('---> limit_value: ' + str(best_result[7]))

        
def main() :
    name_of_file = sys.argv[1]
    max_limit_value = float(sys.argv[2])
    increment = float(sys.argv[3])
    mode = sys.argv[4]
    list_of_data = populate_dataset(name_of_file)
    min_score = min(list_of_data)[0]
    max_score = max(list_of_data)[0]
    print_dataset_info(len(list_of_data), min_score, max_score)
    list_of_metrics = metrics_evaluation(list_of_data, min_score, max_limit_value, increment, mode)
    print('\n')
    print_metrics(max(list_of_metrics))
    print('\n')

if __name__ == '__main__':
    main()
