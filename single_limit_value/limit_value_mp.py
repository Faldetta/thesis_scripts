import sys
import math
from multiprocessing import Process, Queue

processes_no = 10

def populate_dataset(name_of_file) :
    data_list = [] 
    with open(name_of_file, 'r') as file :
        line = file.readline();
        eof = False
        while not eof :
            line = file.readline().split(',')
            if line == ['']:
                eof = True
            else :
                data_list.append([float(line[0]), line[1].strip('"')])
    return data_list

def print_dataset_info(dataset_length, min_score, max_score) :
    print('\n')
    print('---> dataset popolato: ' + str(dataset_length) + ' elementi')
    print('---> score minimo: ' + str(min_score))
    print('---> score massimo: ' + str(max_score))

def higher_anomaly_computation(data_list, limit_value) :
    tp, fp, tn, fn = 0, 0, 0, 0
    for x in data_list :
        if(x[0] > limit_value and (x[1] == 'yes' or x[1] == 'yes\n')) :
            tp = tp + 1
        elif(x[0] > limit_value and (x[1] == 'no' or x[1] == 'no\n')) :
            fp = fp + 1
        elif(x[0] <= limit_value and (x[1] == 'no' or x[1] == 'no\n')) :
            tn = tn + 1
        elif(x[0] <= limit_value and (x[1] == 'yes'or x[1] == 'yes\n')) :
            fn = fn +1
    return tp, fp, tn, fn

def lower_anomaly_computation(data_list, limit_value) :
    tp, fp, tn, fn = 0, 0, 0, 0
    for x in data_list :
        if(x[0] < limit_value and (x[1] == 'yes' or x[1] == 'yes\n')) :
            tp = tp + 1
        elif(x[0] < limit_value and (x[1] == 'no' or x[1] == 'no\n')) :
            fp = fp + 1
        elif(x[0] >= limit_value and (x[1] == 'no' or x[1] == 'no\n')) :
            tn = tn + 1
        elif(x[0] >= limit_value and (x[1] == 'yes' or x[1] == 'yes\n')) :
            fn = fn +1
    return tp, fp, tn, fn

def statistics_computation(data_list, limit_value, mode) :
    if mode == 'high' :
        return higher_anomaly_computation(data_list, limit_value)
    elif mode == 'low' :
        return lower_anomaly_computation(data_list, limit_value)

def metrics_calculus(tp, fp, tn, fn, limit_value) :
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    accuracy = (tp + tn) / (tp + fp + tn + fn)
    f1 = (2 * precision * recall) / (precision + recall)
    f2 = ((1 + (2**2)) * precision * recall) / ((2**2) * precision + recall)
    auc_roc = (tp / (2 * (tp + fn))) + (tn / (2 * (tn + fp)))
    mcc = ((tp * tn) - (fp * fn)) / (math.sqrt((tp + fp)*(tp + fn)*(tn + fp)*(tn + fn)))
    # il primo elemento e' extra, lo uso per cercare il max
    return [mcc , mcc, accuracy, f2, f1, precision, recall, auc_roc, limit_value, tp, fp, tn, fn]

def metrics_computation(data_list, queue, limit_value, mode) :
    tp, fp, tn, fn = statistics_computation(data_list, limit_value, mode)
    if (tp)  * (tn + fp) * (tn + fn) != 0 :
        queue.put(metrics_calculus(tp, fp, tn, fn, limit_value))
    else :
        #print('\n')
        #print('-------> zero division avoided, limit = ' + str(limit_value))
        #print('-------> tp: ' + str(tp))
        #print('-------> fp: ' + str(fp))
        #print('-------> tn: ' + str(tn))
        #print('-------> fn: ' + str(fn))
        queue.put([-1])
        
def metrics_evaluation(data_list, max_limit_value, increment, mode) :
    metrics_list = []
    queue = Queue()
    limit_value = 0
    processes_list = list(range(processes_no))
    while limit_value <= max_limit_value :
        for i in range (0, processes_no):
            processes_list[i] = Process(target = metrics_computation, args = (data_list, queue, limit_value + (i * increment), mode))
        for process in processes_list :
            process.start()
        for process in processes_list :
            metrics_list.append(queue.get())
            process.join()
        limit_value = limit_value + (processes_no * increment)
    return metrics_list
    
def print_metrics(results) :
    print('---> tp: ' + str(results[9]))
    print('---> fp: ' + str(results[10]))
    print('---> tn: ' + str(results[11]))
    print('---> fn: ' + str(results[12]))
    print('\n')
    print('---> precision: ' + str(results[5]))
    print('---> recall: ' + str(results[6]))
    print('---> accuracy: ' + str(results[2]))
    print('---> f1: ' + str(results[4]))
    print('---> f2: ' + str(results[3]))
    print('---> mcc: ' + str(results[1]))
    print('---> auc_roc: ' + str(results[7]))
    print('---> limit_value: ' + str(results[8]))
    
        
def main() :
    name_of_file = sys.argv[1]
    max_limit_value = float(sys.argv[2])
    increment = float(sys.argv[3])
    mode = sys.argv[4]
    data_list = populate_dataset(name_of_file)
   # print(data_list)
    min_score = min(data_list)[0]
    max_score = max(data_list)[0]
    print_dataset_info(len(data_list), min_score, max_score)
    metrics_list = metrics_evaluation(data_list, max_limit_value, increment, mode)
    print('\n')
    print_metrics(max(metrics_list))
    print('\n')

if __name__ == '__main__':
    main()
    
